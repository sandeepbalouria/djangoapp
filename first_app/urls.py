from django.urls import path

from . import views

app_name = 'first_app'

urlpatterns = [
    path('', views.index, name='index'),
    path('formpage/', views.form_view, name='formpage'),
    path('signup/', views.sign_up, name='signup'),
    path('first_app/', views.index, name='index'),
    path('registration/', views.register, name='register'),
]
